package com.ultimate.challenge.repository;

import com.ultimate.challenge.entity.Reply;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface ReplyRepository extends MongoRepository<Reply, String> {
    @Query("{'name' : ?0}")
    Optional<Reply> findReplyByName(String replyName);
}
