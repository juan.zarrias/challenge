package com.ultimate.challenge.controller;

import com.ultimate.challenge.dto.ReplyDTO;
import com.ultimate.challenge.dto.RequestDTO;
import com.ultimate.challenge.service.UltimateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UltimateController {

    private final UltimateService ultimateService;

    public UltimateController(final UltimateService ultimateService) {
        this.ultimateService = ultimateService;
    }

    @PostMapping("/reply")
    public ResponseEntity<ReplyDTO> getReply(final @RequestBody RequestDTO requestDTO){
        return ultimateService.getReply(requestDTO);
    }
}
