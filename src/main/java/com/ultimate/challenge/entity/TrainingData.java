package com.ultimate.challenge.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class TrainingData {
    private Integer expressionCount;
    private List<Message> messages;
    private List<Message> expressions;
}
