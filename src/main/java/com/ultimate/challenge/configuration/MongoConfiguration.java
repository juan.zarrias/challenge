package com.ultimate.challenge.configuration;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class MongoConfiguration  {

    @Value("${ultimate.api.url}")
    private String ultimateApiUrl;

    @Value("${ultimate.api.key}")
    private String ultimateApiKey;

    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create();
    }

    @Bean
    public WebClient webClient() {
        return WebClient.builder().baseUrl(this.ultimateApiUrl).defaultHeaders(httpHeaders -> {
            httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            httpHeaders.add(HttpHeaders.AUTHORIZATION, this.ultimateApiKey);
        }).build();
    }

}
