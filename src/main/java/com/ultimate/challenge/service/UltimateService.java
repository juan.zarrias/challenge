package com.ultimate.challenge.service;

import com.ultimate.challenge.dto.*;
import com.ultimate.challenge.entity.Reply;
import com.ultimate.challenge.repository.ReplyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Optional;

@Service
@Slf4j
public class UltimateService {

    public static final ResponseEntity<ReplyDTO> NOT_FOUND_RESPONSE = ResponseEntity.notFound().build();
    @Value("${ultimate.api.intents}")
    private String ultimateApiIntentsPath;

    @Value("${ultimate.api.confidence.threshold}")
    private double confidenceThreshold;

    private final ReplyRepository replyRepository;

    private final WebClient webClient;

    public UltimateService(final ReplyRepository replyRepository, final WebClient webClient) {
        this.replyRepository = replyRepository;
        this.webClient = webClient;
    }

    public ResponseEntity<ReplyDTO> getReply(final RequestDTO requestDTO) {
        final ResponseEntity<IntentsResponseDTO> apiResponse = this.getUltimateApiIntent(requestDTO);

        if (apiResponse != null)  {
            if(apiResponse.getStatusCode().is2xxSuccessful()) {
                return checkAndReturn(apiResponse.getBody());
            } else {
                return ResponseEntity.badRequest().build();
            }
        } else {
            return NOT_FOUND_RESPONSE;
        }
    }

    private ResponseEntity<IntentsResponseDTO> getUltimateApiIntent(final RequestDTO requestDTO) {

        return webClient.post()
                .uri(ultimateApiIntentsPath)
                .bodyValue(UltimateAPIRequestDTO.of(requestDTO.getBotId(), requestDTO.getMessage()))
                .retrieve()
                .toEntity(IntentsResponseDTO.class)
                .onErrorReturn(ResponseEntity.badRequest().build())
                .block();

    }

    private ResponseEntity<ReplyDTO> checkAndReturn(final IntentsResponseDTO intentsResponseDTO) {

        if (intentsResponseDTO != null) {

            final Optional<IntentDTO> highestIntent = intentsResponseDTO.getIntents().stream().filter(intentDTO -> intentDTO.getConfidence() >= this.confidenceThreshold).findFirst();
            if (highestIntent.isEmpty()) {
                return NOT_FOUND_RESPONSE;
            }

            final Optional<Reply> optionalReply = replyRepository.findReplyByName(highestIntent.get().getName());

            if (optionalReply.isEmpty()) {
                return NOT_FOUND_RESPONSE;
            }

            return ResponseEntity.ok(ReplyDTO.of(optionalReply.get().getReply().getText()));
        }
        return NOT_FOUND_RESPONSE;
    }

}
