package com.ultimate.challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
public class RequestDTO {
    @NotEmpty(message = "botId cannot be empty")
    private final String botId;
    @NotEmpty(message = "message cannot be empty")
    private final String message;
}
