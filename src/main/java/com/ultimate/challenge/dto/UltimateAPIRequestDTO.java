package com.ultimate.challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
public class UltimateAPIRequestDTO {
    private final String botId;
    private final String message;

    public static UltimateAPIRequestDTO of(final String botId, final String message) {
        return new UltimateAPIRequestDTO(botId, message);
    }
}
