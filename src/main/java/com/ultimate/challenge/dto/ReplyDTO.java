package com.ultimate.challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReplyDTO {
    private final String message;

    public static ReplyDTO of(final String text) {
        return new ReplyDTO(text);
    }
}
