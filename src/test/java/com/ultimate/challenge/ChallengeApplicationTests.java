package com.ultimate.challenge;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ultimate.challenge.dto.RequestDTO;
import com.ultimate.challenge.entity.Message;
import com.ultimate.challenge.entity.Reply;
import com.ultimate.challenge.entity.TrainingData;
import com.ultimate.challenge.repository.ReplyRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
class ChallengeApplicationTests {

    public static final String REPLY_ENDPOINT = "/reply";
    public static final String BOT_ID = "5f74865056d7bb000fcd39ff";
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ReplyRepository replyRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void shouldReturnCorrectlyGreetingsIntent() throws Exception {
        //given:
        replyRepository.save(new Reply(
                "34d7831e137a4016a55f98926800a643",
                "Greeting",
                "The visitor says hello.",
                new TrainingData(null,
                        List.of(new Message("6399fd6989984c7b871c6301744b0af5", "Hello"),
                                new Message("68bafebc2a2e4843a56a221c2ceb12ed", "Hi"),
                                new Message("b2a3208dc801432992812638368e0668", "Good morning!")), null),
                new Message("f35d7e0936a44102bac9cb96c81eec3b", "Hello :) How can I help you?")
        ));

        //then:
        mvc.perform(MockMvcRequestBuilders
                .post(REPLY_ENDPOINT)
                .content(asJsonString(new RequestDTO(BOT_ID, "Hello")))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Hello :) How can I help you?"));
    }

    @Test
    void shouldReturnNotFoundWhenDBDoesntContainIntent() throws Exception {
        //expect:
        mvc.perform(MockMvcRequestBuilders
                .post(REPLY_ENDPOINT)
                .content(asJsonString(new RequestDTO(BOT_ID, "Hello")))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

