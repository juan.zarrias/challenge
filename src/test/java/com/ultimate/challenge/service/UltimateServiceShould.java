package com.ultimate.challenge.service;

import com.ultimate.challenge.dto.IntentDTO;
import com.ultimate.challenge.dto.IntentsResponseDTO;
import com.ultimate.challenge.dto.ReplyDTO;
import com.ultimate.challenge.dto.RequestDTO;
import com.ultimate.challenge.entity.Message;
import com.ultimate.challenge.entity.Reply;
import com.ultimate.challenge.entity.TrainingData;
import com.ultimate.challenge.repository.ReplyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "com.ultimate.challenge.service.UltimateService")
public class UltimateServiceShould {

    @Test
    public void shouldReturnCorrectIntent() throws Exception {
        //given:
        final ReplyRepository replyRepository = mock(ReplyRepository.class);
        final WebClient webClient = mock(WebClient.class);
        final UltimateService ultimateService = spy(new UltimateService(replyRepository, webClient));

        //and:
        final IntentsResponseDTO intentsResponseDTOMock = new IntentsResponseDTO(
                List.of(new IntentDTO(1, "Thank you"),
                        new IntentDTO(0.8621754050254822, "Goodbye"),
                        new IntentDTO(0.08200448751449585, "Label for returning"),
                        new IntentDTO(0.04324828088283539, "Returning order"),
                        new IntentDTO(0.08200448751449585, "Greeting")));
        doReturn(ResponseEntity.ok(intentsResponseDTOMock)).when(ultimateService, "getUltimateApiIntent", ArgumentMatchers.any());

        //and:
        final Reply replyMock = new Reply("1234",
                "Thank you",
                "The visitor says thank you.",
                new TrainingData(null,
                        List.of(new Message("71dbc302036d4257bbb6c477e0e0a7fc", "Brilliant! Thanks!"),
                                new Message("0fa23ff354fb41b29da47ecd7fbbdaee", "thx"),
                                new Message("9d8a7691475e45c0b06304b34721ce38", "thank you")),
                        null),
                new Message("17134b01d2e343bc81e48fad4ec2ca00", "It was a pleasure to be of help :)"));
        when(replyRepository.findReplyByName(anyString())).thenReturn(Optional.of(replyMock));

        //when:
        final ResponseEntity<ReplyDTO> reply = ultimateService.getReply(new RequestDTO("botId", "Goodbye"));

        //then:
        assertTrue(reply.getStatusCode().is2xxSuccessful());
        verifyPrivate(ultimateService).invoke("getUltimateApiIntent", ArgumentMatchers.any());
        assertEquals("It was a pleasure to be of help :)", Objects.requireNonNull(reply.getBody()).getMessage());
    }

    @Test
    public void shouldReturnNotFoundWhenUltimateAPIReturnsBadRequest() throws Exception {
        //given:
        final ReplyRepository replyRepository = mock(ReplyRepository.class);
        final WebClient webClient = mock(WebClient.class);
        final UltimateService ultimateService = spy(new UltimateService(replyRepository, webClient));

        //and:
        doReturn(null).when(ultimateService, "getUltimateApiIntent", ArgumentMatchers.any());

        //when:
        final ResponseEntity<ReplyDTO> reply = ultimateService.getReply(new RequestDTO("botId", "Goodbye"));

        //then:
        assertEquals(HttpStatus.NOT_FOUND, reply.getStatusCode());
    }

    @Test
    public void shouldReturnNotFoundWhenIntentNotFoundInDB() throws Exception {
        //given:
        final ReplyRepository replyRepository = mock(ReplyRepository.class);
        final WebClient webClient = mock(WebClient.class);
        final UltimateService ultimateService = spy(new UltimateService(replyRepository, webClient));

        //and:
        final IntentsResponseDTO intentsResponseDTOMock = new IntentsResponseDTO(
                List.of(new IntentDTO(1, "Thank you"),
                        new IntentDTO(0.8621754050254822, "Goodbye"),
                        new IntentDTO(0.08200448751449585, "Label for returning"),
                        new IntentDTO(0.04324828088283539, "Returning order"),
                        new IntentDTO(0.08200448751449585, "Greeting")));
        doReturn(ResponseEntity.ok(intentsResponseDTOMock)).when(ultimateService, "getUltimateApiIntent", ArgumentMatchers.any());

        //and:
        when(replyRepository.findReplyByName(anyString())).thenReturn(Optional.empty());

        //when:
        final ResponseEntity<ReplyDTO> reply = ultimateService.getReply(new RequestDTO("botId", "Goodbye"));

        //then:
        assertEquals(HttpStatus.NOT_FOUND, reply.getStatusCode());
    }

    @Test
    public void shouldReturnBadRequestFromUltimateAPI() throws Exception {
        //given:
        final ReplyRepository replyRepository = mock(ReplyRepository.class);
        final WebClient webClient = mock(WebClient.class);
        final UltimateService ultimateService = spy(new UltimateService(replyRepository, webClient));

        //and:
        doReturn(ResponseEntity.badRequest().build()).when(ultimateService, "getUltimateApiIntent", ArgumentMatchers.any());


        //when:
        final ResponseEntity<ReplyDTO> reply = ultimateService.getReply(new RequestDTO("botId", "Goodbye"));

        //then:
        assertEquals(HttpStatus.BAD_REQUEST, reply.getStatusCode());
    }

}
